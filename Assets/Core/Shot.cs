﻿using UnityEngine;
using System.Collections;

public abstract class Shot : MonoBehaviour
{
    // and abstract class that stores general info about every shot in the game.
    protected Rigidbody rBody; // rigid body belongs to the shot
    protected Transform target;

    public float velocity;
    public float damage; // damage caused to the hitted object on collision
    public float range; // max range of shot

    public void SetTarget(Transform target)
    {
        this.target = target;
    }
    public void SetTarget(GameObject target)
    {
        this.target = target.transform;
    }

    void OnCollisionEnter(Collision col)
    {
        //Spaceship other; // what?
        GameObject collided = col.gameObject;
        Destroy(gameObject);
        Hit(collided.transform.root.GetComponentInChildren<Spaceship>());
    }

    protected void Hit(Spaceship target)
    {
        if (target != null)
        {
            target.GetHit(damage);
        }
    }
}
