﻿using UnityEngine;
using System.Collections;

public class GuidedMissile : Shot
{
    public float turnAngle;
    public string explosionName;
    public float fadeDelay;

    private bool isAlive;
    private GameObject explosionPrefab;

    private Vector3 lastPos;

    void Start()
    {
        Init();
        lastPos = transform.position;
    }

    void FixedUpdate()
    {
        Guide();
        Move();
        CalculateRange();
    }

    public void Init()
    {
        rBody = GetComponent<Rigidbody>();
        rBody.useGravity = false;
        rBody.isKinematic = false;
        isAlive = true;
        explosionPrefab = Resources.Load<GameObject>(explosionName);
    }

    public void Guide()
    {
        if (target != null)
        {
            Quaternion rotationToTarget = Quaternion.LookRotation(target.position - transform.position);
            rBody.MoveRotation(Quaternion.RotateTowards(transform.rotation, rotationToTarget, turnAngle));
        }
    }

    public void Move()
    {
        Vector3 lastPos = transform.position;
        rBody.velocity = transform.forward * velocity;
        if (range <= 0 && isAlive)
        {
            DynamicDestruction();
            isAlive = false;
        }
    }

    // kind of animations to make the disappearing of the object, the smoke and the fire better
    public void DynamicDestruction()
    {
        Vector3 positionForExplosion = gameObject.transform.Find("MissileModel").position; // making the right position for explosion
        GameObject explosion = (GameObject)Instantiate(Resources.Load(explosionName), positionForExplosion, new Quaternion(0, 0, 0, 0));
        float explosionDelay = explosion.GetComponentInChildren<AudioSource>().clip.length;
        GetComponentInChildren<Emitter>().Finish(); // calling out to stop the engine's smoke effect
        GetComponentInChildren<MeshRenderer>().enabled = false; // the missile is out of range, thus it disappears
        velocity = 0; // setting the velocity to 0, to prevent unwanted collisions
        Destroy(gameObject.GetComponentInChildren<Collider>()); // destroying the collider for the same reason
        Destroy(gameObject, fadeDelay); // waiting >fadeDelay seconds for the missile trail to finish its fade-out and destroying self
        Destroy(explosion, explosionDelay); // waiting >explosionDelay seconds for the audio to finish playing before destroying
    }

    // event for hitting another object (Spaceship)
    void OnCollisionEnter(Collision col)
    {
        Spaceship other;
        GameObject collided = col.gameObject;
        other = collided.GetComponent<Spaceship>();
        DynamicDestruction();
        if (other != null)
        {
            Hit(other);
        }
    }

    private void CalculateRange()
    {
        range -= Vector3.Distance(lastPos, transform.position) * 1.5f;
        lastPos = transform.position;
    }
}
