﻿using UnityEngine;
using System.Timers;

public class Weapon : MonoBehaviour
{
    private Timer reloader;
    public float reloadTime;
    public float damage;
    public float range;
    public GameObject prefab;

    void Awake()
    {
        reloader = new Timer(reloadTime);
        reloader.Elapsed += Reloader_Elapsed;
    }

    private void Reloader_Elapsed(object sender, ElapsedEventArgs e)
    {
        reloader.Enabled = false;
    }

    public void Fire(Transform target)
    {
        if (IsReloaded)
        {
            GameObject projectile = (GameObject)Instantiate(prefab, transform.position, transform.rotation);
            Shot shot = projectile.GetComponent<Shot>();
            shot.SetTarget(target);
            reloader.Enabled = true;
        }
    }

    /*public void Fire(Vector3 target)
    {
        if (IsReloaded)
        {

        }
    }*/

    public bool IsReloaded
    {
        get
        {
            return reloader.Enabled == false;
        }
    }
}
