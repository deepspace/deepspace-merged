﻿using UnityEngine;
using System.Collections.Generic;

public class Spaceship : MonoBehaviour
{
    static private float locationDifferenceRadius = 10f;
    public float velocity;
    public float angularVelocity; 
    public float health;
    private Weapon weapon;
    private UISpaceship ui;
    private Rigidbody rBody;
    private Vector3 targetLocation; // location to move to
    private GameObject targetObject; // object to move to / shoot at(if enemy)
    private Command control;
    private SelectableUnit unit;

    void Awake()
    {
        rBody = GetComponent<Rigidbody>();
        control = GameObject.Find("UI").GetComponent<Command>(); // getting the center of the UI system for this game
        weapon = transform.Find("Weapon").GetComponent<Weapon>();
        unit = GetComponent<SelectableUnit>();
    }

    void FixedUpdate()
    {
        Move();
        Attack();
        if (tag == "me")
        {
            MarkTargetInRange();
        }
    }

    public void Move()
    {
        Guide();
        if (Vector3.Distance(transform.position, targetLocation) > locationDifferenceRadius)
        {
            rBody.velocity = transform.forward * velocity;
        }
        else // just effect to make the spaceship look forward once stopped
        {
            rBody.freezeRotation = true;
            Vector3 forward = transform.forward;
            forward.y = 0;
            Quaternion rotationToTarget = Quaternion.LookRotation(forward);
            rBody.MoveRotation(Quaternion.RotateTowards(transform.rotation, rotationToTarget, angularVelocity));
            rBody.velocity = Vector3.zero;
        }
    }

    public void Guide()
    {
        Quaternion rotationToTarget = Quaternion.LookRotation(targetLocation - transform.position);
        transform.rotation = (Quaternion.RotateTowards(transform.rotation, rotationToTarget, angularVelocity));
    }

    void OnMouseDown()
    {
        control.SelectObject(gameObject);
    }

    public void Attack()
    {
        float range;
        if (targetObject != null)
        {
            range = Vector3.Distance(targetObject.transform.position, transform.position);
            if (range <= weapon.range)
            {
                weapon.Fire(targetObject.transform);
            }
        }
    }

    public void MarkTargetInRange()
    {
        Spaceship hit;
        Collider[] hitColliders;
        if (targetObject == null && weapon != null)
        {
            hitColliders = Physics.OverlapSphere(transform.position, weapon.range);
            for (int i = 0; i < hitColliders.Length; i++)
            {
                hit = hitColliders[i].transform.root.GetComponentInChildren<Spaceship>();
                if (hit != null)
                {
                    if (hit.tag == "enemy")
                    {
                        targetObject = hitColliders[i].transform.root.gameObject;
                    }
                }
            }
        }
    }

    public float Angle(Transform other)
    {
        Vector3 dir = other.transform.position - transform.position;
        dir = other.transform.InverseTransformDirection(dir);
        return Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
    }

    public void SetDestination(Vector3 dest)
    {
        targetLocation = dest;
        targetLocation.y = control.altitude;
    }

    public void GetHit(float damage)
    {
        health -= damage;
    }

    public void SetUISpaceshipInfo(UISpaceship ui)
    {
        this.ui = ui;
    }
    public void SetHealth(int health)
    {
        this.health = health;
    }
    public float Health
    {
        get
        {
            return health;
        }
    }
    public bool IsSelected
    {
        get
        {
            return unit.isSelected;
        }
    }
}
