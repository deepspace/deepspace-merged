﻿using UnityEngine;
using System.Linq;
using System.Collections;

public class ReverseNormals : MonoBehaviour
{
    // Use this for initialization
    void Start ()
    {
        Transform transform = GetComponent<Transform>();
        Mesh mesh = GetComponent<MeshFilter>().mesh;
        mesh.triangles = mesh.triangles.Reverse().ToArray();
        transform.position = new Vector3(0, 0, 0);
    }
	
	// Update is called once per frame
	void Update ()
    {
	
	}
}
