﻿using UnityEngine;
using System.Collections;

public class SetColorByTeam : MonoBehaviour
{
    private Color color;
    private Command control;
    private Spaceship ship;

    void Start()
    {
        control = GameObject.Find("UI").GetComponent<Command>(); // getting the center of the UI system for this game
        ship = transform.root.gameObject.GetComponent<Spaceship>();
    }

	void Update()
    {
        if (transform.root.tag == "me")
        {
            color = control.myColor;
            if (ship.IsSelected)
            {
                color = Color.white;
            }
        }
        if (transform.root.tag == "enemy")
        {
            color = control.opponentColor;
        }
        color.a = 0.1f;
        GetComponent<ParticleSystem>().startColor = color;
    }
}
