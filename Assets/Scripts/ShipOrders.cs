﻿using UnityEngine;
using System.Collections;

public class ShipOrders : MonoBehaviour
{
    private Command control;

	// Use this for initialization
	void Start ()
    {
    }
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    /// <summary>
    /// event handler function
    /// the function works for left mouse button clicks only!
    /// </summary>
    

    /// <summary>
    /// this function was just to test missile
    /// the missiles are fired from below the camera, towards the dummy-ship -> Ship1
    /// </summary>
    private void TestingMissile(string shipName)
    {
        float x = GameObject.Find("Main Camera").transform.position.x;
        float y = GameObject.Find("Main Camera").transform.position.y;
        float z = GameObject.Find("Main Camera").transform.position.z;
        GameObject newMissile = (GameObject)Instantiate(Resources.Load<GameObject>("Missile1"), new Vector3(x, y - 50, z), GameObject.Find("Main Camera").transform.rotation);
        GuidedMissile script = newMissile.GetComponent<GuidedMissile>();
        script.SetTarget(GameObject.Find(shipName));
    }
}
