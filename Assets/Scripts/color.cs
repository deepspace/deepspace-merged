﻿using UnityEngine;
using System.Collections;

public class color : MonoBehaviour
{
    float h;
    float w;
    int imageX = 3;
    int imageZ = 3;
    // Use this for initialization
    void Start () {
        Resize();
        h = Screen.height;
        w = Screen.width;
        //this.gameObject.GetComponent<Renderer>().material.color = new Color(0, 0, 0);
    }
	
	// Update is called once per frame
	void Update () {
        if (h != Screen.height || w != Screen.width) // if the player changes the size of the screen.
        {
            Resize();
            h = Screen.height;
            w = Screen.width;
        }

    }
    private void Resize()
    {
        float worldScreenHeight;
        float worldScreenWidth;
        if (Screen.height/imageZ>Screen.width/imageX) // changing the size according to the width.
        {
            worldScreenWidth = Camera.main.orthographicSize * 2;
            worldScreenHeight = worldScreenWidth / Screen.width * Screen.height;
        }
        else // changing the size according to the hieght.
        {
            worldScreenHeight = Camera.main.orthographicSize * 2;
            worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;
        }
         
        transform.localScale = new Vector3(worldScreenWidth / imageX, 1,worldScreenHeight / imageZ);
    }
}
