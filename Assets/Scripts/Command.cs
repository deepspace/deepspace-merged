﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Command : MonoBehaviour
{
    private GameObject currentlySelected;
    public float altitude;

    public Color myColor;
    public Color opponentColor;

    public GameObject myWarp;
    public GameObject opponentWarp;

    public GameObject selectionPrefab;

    public GameObject grid;

    public List<GameObject> spaceships;

	// Use this for initialization
	void Start ()
    {
        if (Network.isServer)
        {
            myWarp = GameObject.Find("Warp1");
            opponentWarp = GameObject.Find("Warp2");
        }
        else if (Network.isClient)
        {
            myWarp = GameObject.Find("Warp2");
            opponentWarp = GameObject.Find("Warp1");
        }

        spaceships = new List<GameObject>();
        grid = GameObject.Find("Grid");

        myWarp.GetComponent<ParticleSystem>().startColor = myColor; // coloring my warp
        myWarp.GetComponentInChildren<LensFlare>().color = myColor;

        opponentWarp.GetComponent<ParticleSystem>().startColor = opponentColor; // coloring opponent warp
        opponentWarp.GetComponentInChildren<LensFlare>().color = opponentColor;
    }

    /// <summary>
    /// event handler sub-function - connects between player oreders and game object
    /// </summary>
    /// <param name="obj">The selected game object</param>
    public void SelectObject(GameObject obj)
    {
        this.currentlySelected = obj;
    }

    // old mouse selection and ordering system
    public void HandleMouseClick(RaycastHit hit, int button)
    {
        if (button == 1)
        {
            Vector3 target = hit.point;
            target.y = altitude;
            for (int i = 0; i < spaceships.Count; i++)
            {
                Spaceship script = spaceships[i].GetComponent<Spaceship>();
                if (script.IsSelected)
                {
                    script.SetDestination(target);
                    target.x += 100;
                    target.y += 100;
                }
            }
            Instantiate(selectionPrefab, target, new Quaternion());
        }
    }

    public GameObject CurrentlySelected
    {
        get
        {
            return currentlySelected;
        }
    }

}
