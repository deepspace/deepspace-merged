﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class ConstructionBar : MonoBehaviour
{
    private int tabs_count = 0;
    static private bool is_construction_bar_open = false;
    static private List<ConstructionTab> ui_construction_menu;

    public void AttachButtonToUISpaceship(UISpaceship ship)
    {
        RectTransform construcion_button = gameObject.GetComponent<RectTransform>();
        Canvas canvas = gameObject.GetComponentInParent<Canvas>();
        GameObject button = CreateButton(canvas.transform, construcion_button.transform.position, construcion_button.rect, ship);
        OrderButton(button, construcion_button.rect);
        tabs_count++;
    }
    
    public void Start()
    {
        ui_construction_menu = new List<ConstructionTab>();
        AttachButtonToUISpaceship(UISpaceship.Ship1);
        SetTabs();
    }

    public void SetTabs()
    {
        for (int i = 0; i < ui_construction_menu.Count; i++)
        {
            ui_construction_menu[i].Button.SetActive(is_construction_bar_open);
        }
    }

    public void onClick()
    {
        is_construction_bar_open = !is_construction_bar_open;
        SetTabs();
    }

    public GameObject CreateButton(Transform panel, Vector3 position, Rect rect, UISpaceship ship)
    {
        // creating all required objects
        ConstructionTab tab;
        GameObject button_gameobject = new GameObject();
        Button button = button_gameobject.AddComponent<Button>();
        Sprite sprite = Resources.Load<Sprite>("BoxButton");
        Image image = button_gameobject.AddComponent<Image>();
        GameObject text_gameobject = Instantiate(GameObject.Find("Text-CB"));
        Text text;

        // setting the 'parent' of the button to be the GameObject that includes it
        button.transform.SetParent(panel);

        // setting the Image sub-componenet of the button to the image component that has the Sprite loaded
        button.image = image;
        image.sprite = sprite;
        button.targetGraphic = image;

        // making the anchors of the button's image to be the top, left corner
        button.image.rectTransform.anchorMax = new Vector2(0, 1);
        button.image.rectTransform.anchorMin = new Vector2(0, 1);
        button.image.rectTransform.pivot = new Vector2(0.5f, 0.5f);

        // setting the position and size of the button's image
        button_gameobject.transform.position = new Vector3(position.x, position.y);
        button.transform.localScale = new Vector3(1, 1, 1);
        button.image.rectTransform.sizeDelta = rect.size;

        // setting the game object's name and layer to be UI, setting the text of the button
        button_gameobject.layer = 5;
        button_gameobject.name = ship.Name;

        // adding text to the button
        text_gameobject.transform.SetParent(button_gameobject.transform);
        text_gameobject.transform.localScale = GameObject.Find("Text-CB").transform.localScale;
        text = text_gameobject.GetComponent<Text>();
        text.rectTransform.localPosition = Vector3.zero; // this line together with the next line fix
        text.rectTransform.sizeDelta = Vector2.zero; // the positioning for the text on top of the button
        text.text = ship.Name;

        // creating the new tab
        tab = new ConstructionTab(ship, button_gameobject);

        // adding the 'onClick' event to the production tabs
        button.onClick.AddListener(() => tab.onClick());

        // adding the button to the list of button
        ui_construction_menu.Add(tab);

        return button_gameobject;
    }

    public void OrderButton(GameObject button_gameobject, Rect rect)
    {
        // using the local reference in positioning the button, below the construction button
        button_gameobject.transform.localPosition = new Vector3(button_gameobject.transform.localPosition.x + rect.width * tabs_count, button_gameobject.transform.localPosition.y - rect.height);
    }
}