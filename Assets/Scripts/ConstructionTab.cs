﻿using UnityEngine;
using System.Collections;

public class ConstructionTab : MonoBehaviour
{
    private UISpaceship uiship;
    private GameObject button;
    private Command control;

    // this script should be used very carefully!
    // since it is almost the only one to be used with constructor insted of Start, which is not working ever!
    public ConstructionTab(UISpaceship ship, GameObject button)
    {
        this.uiship = ship;
        this.button = button;

        control = GameObject.Find("UI").GetComponent<Command>();
    }

    public UISpaceship Ship
    {
        get
        {
            return this.uiship;
        }
    }
    public GameObject Button
    {
        get
        {
            return this.button;
        }
    }

    public void onClick()
    {
        CreateShip(Network.player);
    }

    public void CreateShip(NetworkPlayer player)
    {
        GameObject newShip = (GameObject)Network.Instantiate(uiship.Prefab, control.myWarp.transform.position, control.myWarp.transform.rotation, 1);
        Spaceship script = newShip.GetComponentInChildren<Spaceship>();
        script.SetDestination(control.myWarp.transform.Find("Spawn").transform.position);
        script.enabled = true; // making 100% sure that the script is activated!
        RegisterShip(newShip, "me");
    }

    public void RegisterShip(GameObject ship, string tag)
    {
        ship.tag = tag; // setting the ship to have the tag of the current player
        control.spaceships.Add(ship);
    }
}
