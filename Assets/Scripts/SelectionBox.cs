﻿using UnityEngine;
using System.Collections;

public class SelectionBox : MonoBehaviour
{
    public Texture2D texture;
    static public Rect selectionRect = new Rect(0, 0, 0, 0);
    private Vector3 startPos = -Vector3.one;

    void Update()
    {
        Select();
    }

    private void Select()
    {
        if (Input.GetMouseButtonDown(0))
        {
            startPos = Input.mousePosition;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            if (selectionRect.width < 0)
            {
                selectionRect.x += selectionRect.width;
                selectionRect.width = -selectionRect.width;
            }
            if (selectionRect.height < 0)
            {
                selectionRect.y += selectionRect.height;
                selectionRect.height = -selectionRect.height;
            }
            startPos = -Vector3.one;
        }

        if (Input.GetMouseButton(0))
        {
            float width = Input.mousePosition.x - startPos.x;
            float height = ScreenToMouse(Input.mousePosition.y) - ScreenToMouse(startPos.y);
            selectionRect = new Rect(startPos.x, ScreenToMouse(startPos.y), width, height);
        }
    }

    private void OnGUI()
    {
        if (startPos != -Vector3.one)
        {
            GUI.color = new Color(1, 1, 1, 0.5f);
            GUI.DrawTexture(selectionRect, texture);
        }
    }

    // the thing with Y is that it is relative to the top of the screen
    // we want it to be relative to the bottom, because we drag it downwards
    static public float ScreenToMouse(float y)
    {
        return Screen.height - y;
    }
}
