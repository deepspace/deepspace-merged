﻿using UnityEngine;
using System.Collections.Generic;
using System;
public class message {

    private string CODE; // will hold the code: "CreateRoom","DeleteRoom", ...
    private List<string> parameters=null;

    public message(string msg)
    {
        if (this.isValid(msg)) // only if the msg is valid.
        { 
            this.CODE = getCodeFromMSG(msg);
            this.parameters = getParametersFromMSG(msg);
        }
        else
        {
            this.CODE = "Syntax error";
            this.parameters = null;
        }
    }
    private string getCodeFromMSG(string msg)
    {
        return msg.Split('|')[0]; // the code is the first argument.
    }
    private List<string> getParametersFromMSG(string msg)
    {
        string[] all = msg.Split('|');
        List<string> onlyParameters = new List<string>();
        for (int i=0;i< all.Length - 3; i++) // for example "CreateRoom|room1||" after split -> ["CreateRoom","room1","",""]
        {
            onlyParameters.Add(all[i + 1]);
        }
        return onlyParameters;
    }

    private bool isValid(string msg)
    { // checking the given string.
        bool returnValue = false;
        const int NUM = 4;
        if (msg == null || !msg.Contains("|") || msg.Length < 6 || !isEndWith(msg,"||\0")) // 6 == "BYE||\0".Length 
        {
            returnValue = false;
        }
        else if (msg.StartsWith("CreateRoom|"))
        {
            if (msg.IndexOf("|") < msg.Length - NUM)
                returnValue = true;
            else
                returnValue = false;
        }
        else if (msg.StartsWith("DeleteRoom|"))
        {
            if (msg.IndexOf("|") < msg.Length - NUM)
                returnValue = true;
            else
                returnValue = false;
        }
        else if (msg.StartsWith("GetList|"))
        {
            if (msg.IndexOf("|") < msg.Length - NUM)
                returnValue = true;
            else
                returnValue = false;
        }
        else if (msg.StartsWith("JoinToRoom|"))
        {
            if (msg.IndexOf("|") < msg.Length - NUM)
                returnValue = true;
            else
                returnValue = false;
        }
        else if (msg.StartsWith("Joined|"))
        {
            int count = 0;
            for(int i=0;i<msg.Length;i++)
            {
                if (msg[i] == '|')
                    count++;
            }
            if (count == 5)
                returnValue = true;
            else
                returnValue = false;
        }
        else if (msg.StartsWith("Error|"))
            returnValue = true;
        else if (msg.StartsWith("BYE||\0"))
            returnValue = true;
        return returnValue;
    }
    private bool isEndWith(string msg,string str)
    {
        bool returnValue = (msg.Length==0?false:true);
        int count = 0;
        for(count = 0; count < msg.Length && msg[count] != '\0'; count++) { }
        for (int i = count; count - i < str.Length && returnValue; i--)
        {
            if (msg[i] != str[str.Length -1 -(count - i)])
                returnValue = false;
        }
        return returnValue;
    }

    public string getCode()
    {
        return CODE;
    }
    public List<string> getParameters()
    {
        return parameters;
    }
}
