﻿using UnityEngine;
using System.Collections;

public class SelectableUnit : MonoBehaviour
{
    public bool isSelected = false; // improved selection engine
    private Vector3 positionOnCam;

    void Update ()
    {
        if (Input.GetMouseButton(0))
        {
            positionOnCam = Camera.main.WorldToScreenPoint(transform.position);
            positionOnCam.y = SelectionBox.ScreenToMouse(positionOnCam.y);
            isSelected = SelectionBox.selectionRect.Contains(positionOnCam);
        }
	}
}
