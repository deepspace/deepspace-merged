﻿using UnityEngine;
using System.Collections;

public class UISpaceship
{
    private string name;
    private GameObject prefab;
    public UISpaceship(string name)
    {
        prefab = Resources.Load<GameObject>(name);
        this.name = name;
    }
    public string Name
    {
        get
        {
            return this.name;
        }
    }
    public GameObject Prefab
    {
        get
        {
            return this.prefab;
        }
    }

    static public UISpaceship Ship1
    {
        get
        {
            return new UISpaceship("Ship1");
        }
    }
}
