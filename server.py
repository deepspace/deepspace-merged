import socket
import threading

class Rooms:
    def __init__(self):
        self.allRooms=[] # will hold (RommsName,IP,port,cliend_socket)

    def __del__(self):
        print"",

    def createRoom(self,name,ip,port,client_soc):
        for _name,_ip,_port,_client_soc in self.allRooms:
            if name == _name: # the room exists.
                return "CreateRoom|The name "+name+" exists!||"
            if ip == _ip and port==_port: # the person who tries to create the room is already in a room!
                return "CreateRoom|You are already in a room!||"
        self.allRooms.append((name,ip,port,client_soc))
        return "CreateRoom|OK||"

    def deleteRoom(self,name,ip,port):
        for _name,_ip,_port,_cliens_soc in self.allRooms:
            if name==_name:
                if _ip==ip and _port==port: # the person who created a room can delete it.
                    self.allRooms.remove((name,ip,port,_cliens_soc))
                    return "DeleteRoom|OK||"
                else:
                    return "DeleteRoom|You can't delete this room becuase you didn't opend it!||"
        return "DeleteRoom|Room doesn't exists!||"

    def getRooms(self):
        return self.allRooms

    def getRoomsNames(self): # Format: "GetList|name1|name2|...||" or "GetList|No Room avilable!||"
        names = "GetList|"
        for name,ip,port,client_soc in self.allRooms:
            names+=name+"|"
        if names=="GetList|":
            names="GetList|No Room available!|"
        names+="|"
        return names

    def joinToRoom(self,name,ip,port):
        for _name,_ip,_port,_client_soc in self.allRooms:
            if name==_name:
                if ip==_ip and port==_port:
                    return["JoinToRoom|You can't join to yourself!||"]
                self.allRooms.remove((_name,_ip,_port,_client_soc)) # the room can be deleted now.
                return ["Joined|Room:"+_name+"|IP:"+_ip+"|Port:"+str(_port)+"||",_client_soc]
        return ["JoinToRoom|Room doesn't exists||"]

class Network:
    def __init__(self):
        self.ACCEPT_CLIENTS = True
        self.serv_soc = socket.socket()
        self.serv_soc.bind(("0.0.0.0",8080)) # listens on port 8080
        self.startThread = None
        self.all_clients=[]
        self.all_rooms = Rooms()

    def __del__(self):
        for i,address in self.all_clients: # joins all threads
            if i.isAlive():
                print "waiting for",address
                i.join()
        if(self.startThread.isAlive()):
            self.startThread._Thread__stop()
        self.serv_soc.close()

    def start(self):
        self.startThread = threading.Thread(target = self.real_start,args = ())
        self.startThread.start()

    def real_start(self):
        while self.ACCEPT_CLIENTS:
            self.serv_soc.listen(1)
            client_soc, client_address= self.serv_soc.accept()
            #when you get a client, you starts the protocol with him/her.
            self.all_clients.append([threading.Thread(target = self.handle_with_client,args = (client_soc,client_address)),client_address])
            self.all_clients[-1][0].start()

    def stop_accepting(self):
        inp = raw_input("Enter 'shut down' to stop the server: \n")
        while(inp.lower() != "shut down"):
            inp = raw_input()
        self.ACCEPT_CLIENTS = False
        if self.startThread.isAlive():
            self.startThread._Thread__stop() # stop accepting clients!

    def continue_accepting(self):
        self.ACCEPT_CLIENTS = True
        if not self.startThread.isAlive():
            self.startThread = threading.Thread(target = self.real_start,args = ())
            self.startThread.start()

    """
    MESSEGE:
    BYE||\0 - disconnects
    GetList||\0 - returns all rooms
    JoinToRoom|roomName||\0 - joins the player to the room
    CreateRoom|roomName||\0 - creates a room
    DeleteRoom|roomName||\0 - deletes the room
    """
    def handle_with_client(self,client_soc,client_address):
        print "Client:",client_address,"connected."
        sendStr=""
        try:
            info = client_soc.recv(1024)
        except:
            client_soc.close()
            print "Client:",client_address,": connection lost."
            return;
        while info!="BYE||\0":
            print "got:",info
            info = self.splitMsg(info)
            if info[0] == "GetList":
                sendStr=self.all_rooms.getRoomsNames()+"\0"
            elif info[0] == "CreateRoom":
                if len(info)!=2:
                    sendStr="CreateRoom|Error:\n\tCreateRoom needs 1 argument!||"+"\0"
                else:
                    sendStr=self.all_rooms.createRoom(info[1],client_address[0],client_address[1],client_soc)+"\0"
            elif info[0] == "DeleteRoom":
                if len(info)!=2:
                    sendStr="DeleteRoom|Error:\n\tDeleteRoom needs 1 argument!||"+"\0"
                else:
                    sendStr=self.all_rooms.deleteRoom(info[1],client_address[0],client_address[1])+"\0"
            elif info[0] == "JoinToRoom":
                if len(info)!=2:
                    sendStr="JoinToRoom|JoinToRoom needs 1 argument!||"+"\0"
                else:
                    roomName=info[1]
                    info =  self.all_rooms.joinToRoom(info[1],client_address[0],client_address[1])
                    if info[0].startswith("JoinToRoom|"):
                        sendStr=info[0]+"\0"
                    else:
                        sendStr=info[0]+"\0"
                        try:
                            info[1].send("Joined|Room:"+roomName+"|IP:"+client_address[0]+"|Port:"+str(client_address[1])+"||\0")
                        except:
                            print "Client:",client_address,": connection lost."
                            info[1].close()
            else:
                sendStr="Error|Error:\n\tinvalid syntax:\n\t\t\"<code>|<argvs>||\\0\"||\0"
            try:
                client_soc.send(sendStr)
                info = client_soc.recv(1024)
            except:
                client_soc.close()
                print "Client:",client_address,": connection lost."
                return;
            print "sent: ",sendStr
        client_soc.send("BYE||\0")
        print "Client:",client_address,"disconnected."
        client_soc.close()

    def splitMsg(self,info):
        info=str(info)
        if(len(info)<10 or info[-1] != "\0" or info[-2] != "|" or info.count('|')<2):
            return [""]
        newInfo=[]
        infoSoFar=""
        for i in xrange(len(info)-2):
            if info[i]!="|":
                infoSoFar+=info[i]
            else:
                newInfo.append(infoSoFar)
                infoSoFar=""
        return newInfo


def main():
    network = Network()
    network.__init__()
    network.start()
    network.stop_accepting()
    network.__del__()
    del network

if __name__ == "__main__":
	main()
